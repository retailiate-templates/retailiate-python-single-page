import logging

from retailiate_core.core import Retailiate
from starlette.requests import Request

from retailiate_core.utils.rest_api import Context


async def handle(request: Request, retailiate: Retailiate, context: Context):
    """handle a request to the function
    Args:
        req: request body
    """

    return request
